package facade

import (
	"context"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/application/service"
	"gitlab.com/bishe-projects/middle_rank_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"
)

var RankListFacade = new(RankList)

type RankList struct{}

func (f *RankList) Create(ctx context.Context, req *rank.CreateRankListReq) *rank.CreateRankListResp {
	resp := rank.NewCreateRankListResp()
	err := service.RankListApp.Create(ctx, assembler.ConvertCreateRankListReqToRankListEntity(req))
	if err != nil {
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *RankList) Del(ctx context.Context, req *rank.DeleteRankListReq) *rank.DeleteRankListResp {
	resp := rank.NewDeleteRankListResp()
	err := service.RankListApp.Del(ctx, assembler.ConvertDeleteRankListReqToRankListEntity(req))
	if err != nil {
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *RankList) Update(ctx context.Context, req *rank.UpdateRankListReq) *rank.UpdateRankListResp {
	resp := rank.NewUpdateRankListResp()
	err := service.RankListApp.Update(ctx, assembler.ConvertUpdateRankListReqToRankListEntity(req), req.Member, req.Delta)
	if err != nil {
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *RankList) Get(ctx context.Context, req *rank.GetRankListReq) *rank.GetRankListResp {
	resp := rank.NewGetRankListResp()
	rankList, err := service.RankListApp.Get(ctx, assembler.ConvertGetRankListReqToRankListEntity(req), req.Start, req.Length)
	if err != nil {
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.RankList = assembler.ConvertRankListEntityToRankList(rankList)
	return resp
}

func (f *RankList) GetList(ctx context.Context, req *rank.GetRankListsReq) *rank.GetRankListsResp {
	resp := rank.NewGetRankListsResp()
	rankLists, err := service.RankListApp.GetList(ctx, req.Key, req.BusinessId)
	if err != nil {
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.RankLists = rankLists
	return resp
}

func (f *RankList) RemoveMember(ctx context.Context, req *rank.RemoveRankListMemberReq) *rank.RemoveRankListMemberResp {
	resp := rank.NewRemoveRankListMemberResp()
	err := service.RankListApp.RemoveMember(ctx, assembler.ConvertRemoveRankListMemberReqToRankListEntity(req), req.Member)
	if err != nil {
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}
