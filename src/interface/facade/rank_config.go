package facade

import (
	"context"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/application/service"
	"gitlab.com/bishe-projects/middle_rank_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"
)

var RankConfigFacade = new(RankConfig)

type RankConfig struct{}

func (f *RankConfig) Create(ctx context.Context, req *rank.CreateRankConfigReq) *rank.CreateRankConfigResp {
	resp := rank.NewCreateRankConfigResp()
	err := service.RankConfigApp.Create(ctx, assembler.ConvertRankConfigToRankConfigEntity(req.Config))
	if err != nil {
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *RankConfig) Del(ctx context.Context, req *rank.DeleteRankConfigReq) *rank.DeleteRankConfigResp {
	resp := rank.NewDeleteRankConfigResp()
	err := service.RankConfigApp.Del(ctx, assembler.ConvertDeleteRankConfigReqToConfigEntity(req))
	if err != nil {
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *RankConfig) Get(ctx context.Context, req *rank.GetRankConfigReq) *rank.GetRankConfigResp {
	resp := rank.NewGetRankConfigResp()
	config, err := service.RankConfigApp.Get(ctx, assembler.ConvertGetRankConfigReqToConfigEntity(req))
	if err != nil {
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.RankConfig = assembler.ConvertRankConfigEntityToRankConfig(config)
	return resp
}

func (f *RankConfig) GetList(ctx context.Context, req *rank.GetRankConfigsReq) *rank.GetRankConfigsResp {
	resp := rank.NewGetRankConfigsResp()
	nameList, err := service.RankConfigApp.GetList(ctx, req.BusinessId)
	if err != nil {
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.RankConfigs = nameList
	return resp
}
