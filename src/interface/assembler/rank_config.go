package assembler

import (
	"gitlab.com/bishe-projects/middle_rank_service/src/domain/rank_config/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"
)

func ConvertRankConfigToRankConfigEntity(config *rank.RankConfig) *entity.RankConfig {
	return &entity.RankConfig{
		Key:        config.Key,
		BusinessId: config.BusinessId,
		Limit:      config.Limit,
		IsBigTop:   config.IsBigTop,
	}
}

func ConvertRankConfigEntityToRankConfig(entity *entity.RankConfig) *rank.RankConfig {
	return &rank.RankConfig{
		Key:        entity.Key,
		BusinessId: entity.BusinessId,
		Limit:      entity.Limit,
		IsBigTop:   entity.IsBigTop,
	}
}

func ConvertDeleteRankConfigReqToConfigEntity(req *rank.DeleteRankConfigReq) *entity.RankConfig {
	return &entity.RankConfig{
		Key:        req.Key,
		BusinessId: req.BusinessId,
	}
}

func ConvertGetRankConfigReqToConfigEntity(req *rank.GetRankConfigReq) *entity.RankConfig {
	return &entity.RankConfig{
		Key:        req.Key,
		BusinessId: req.BusinessId,
	}
}
