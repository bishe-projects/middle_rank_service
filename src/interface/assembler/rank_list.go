package assembler

import (
	"gitlab.com/bishe-projects/middle_rank_service/src/domain/rank_list/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"
)

func ConvertCreateRankListReqToRankListEntity(req *rank.CreateRankListReq) *entity.RankList {
	return &entity.RankList{
		Key:        req.Key,
		Name:       req.Name,
		BusinessId: req.BusinessId,
	}
}

func ConvertDeleteRankListReqToRankListEntity(req *rank.DeleteRankListReq) *entity.RankList {
	return &entity.RankList{
		Key:        req.Key,
		Name:       req.Name,
		BusinessId: req.BusinessId,
	}
}

func ConvertUpdateRankListReqToRankListEntity(req *rank.UpdateRankListReq) *entity.RankList {
	return &entity.RankList{
		Key:        req.Key,
		Name:       req.Name,
		BusinessId: req.BusinessId,
	}
}

func ConvertGetRankListReqToRankListEntity(req *rank.GetRankListReq) *entity.RankList {
	return &entity.RankList{
		Key:        req.Key,
		Name:       req.Name,
		BusinessId: req.BusinessId,
	}
}

func ConvertRemoveRankListMemberReqToRankListEntity(req *rank.RemoveRankListMemberReq) *entity.RankList {
	return &entity.RankList{
		Key:        req.Key,
		Name:       req.Name,
		BusinessId: req.BusinessId,
	}
}

func ConvertRankItemFromRankItemEntity(rankItem *entity.RankItem) *rank.RankItem {
	return &rank.RankItem{
		Member: rankItem.Member,
		Score:  rankItem.Score,
	}
}

func ConvertRankItemsFromRankItemsEntity(rankItemsEntity []*entity.RankItem) []*rank.RankItem {
	rankItems := make([]*rank.RankItem, 0, len(rankItemsEntity))
	for _, rankItemEntity := range rankItemsEntity {
		rankItems = append(rankItems, ConvertRankItemFromRankItemEntity(rankItemEntity))
	}
	return rankItems
}

func ConvertRankListEntityToRankList(rankList *entity.RankList) *rank.RankList {
	return &rank.RankList{
		Key:        rankList.Key,
		Name:       rankList.Name,
		BusinessId: rankList.BusinessId,
		RankItems:  ConvertRankItemsFromRankItemsEntity(rankList.RankItems),
	}
}
