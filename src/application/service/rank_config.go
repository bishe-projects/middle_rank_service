package service

import (
	"context"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/domain/rank_config/entity"
	"gitlab.com/bishe-projects/middle_rank_service/src/domain/rank_config/service"
)

var RankConfigApp = new(RankConfig)

type RankConfig struct{}

func (a *RankConfig) Create(ctx context.Context, config *entity.RankConfig) *business_error.BusinessError {
	return service.RankConfigDomain.Create(ctx, config)
}

func (a *RankConfig) Del(ctx context.Context, config *entity.RankConfig) *business_error.BusinessError {
	return service.RankConfigDomain.Del(ctx, config)
}

func (a *RankConfig) Get(ctx context.Context, config *entity.RankConfig) (*entity.RankConfig, *business_error.BusinessError) {
	return service.RankConfigDomain.Get(ctx, config)
}

func (a *RankConfig) GetList(ctx context.Context, businessId int64) ([]string, *business_error.BusinessError) {
	return service.RankConfigDomain.GetList(ctx, businessId)
}
