package service

import (
	"context"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/domain/rank_list/entity"
	"gitlab.com/bishe-projects/middle_rank_service/src/domain/rank_list/service"
)

var RankListApp = new(RankList)

type RankList struct{}

func (a *RankList) Create(ctx context.Context, rankList *entity.RankList) *business_error.BusinessError {
	return service.RankListDomain.Create(ctx, rankList)
}

func (a *RankList) Del(ctx context.Context, rankList *entity.RankList) *business_error.BusinessError {
	return service.RankListDomain.Del(ctx, rankList)
}

func (a *RankList) Update(ctx context.Context, rankList *entity.RankList, member string, delta float64) *business_error.BusinessError {
	return service.RankListDomain.Update(ctx, rankList, member, delta)
}

func (a *RankList) Get(ctx context.Context, rankList *entity.RankList, start, length int64) (*entity.RankList, *business_error.BusinessError) {
	return service.RankListDomain.Get(ctx, rankList, start, length)
}

func (a *RankList) GetList(ctx context.Context, key string, businessId int64) ([]string, *business_error.BusinessError) {
	return service.RankListDomain.GetList(ctx, key, businessId)
}

func (a *RankList) RemoveMember(ctx context.Context, rankList *entity.RankList, member string) *business_error.BusinessError {
	return service.RankListDomain.RemoveMember(ctx, rankList, member)
}
