package repo

import (
	"context"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo/po"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo/redis_repo"
)

type RankListDao interface {
	Create(ctx context.Context, rankList *po.RankList) error
	Del(ctx context.Context, rankList *po.RankList) error
	Update(ctx context.Context, rankList *po.RankList, member string, delta float64) error
	Get(ctx context.Context, rankList *po.RankList, start, end int64) (*po.RankList, error)
	GetList(ctx context.Context, key string, businessId int64) ([]string, error)
	RemoveMember(ctx context.Context, rankList *po.RankList, member string) error
}

var RankListRepo = NewRankList(redis_repo.RankListDao)

type RankList struct {
	RankListDao RankListDao
}

func NewRankList(rankListDao RankListDao) *RankList {
	return &RankList{
		RankListDao: rankListDao,
	}
}

//func (r *RankList) CreateRankList(ctx context.Context, key, name string, businessId int64) *business_error.BusinessError {
//	err := redis_repo.RankListDao.Create(ctx, key, name, businessId)
//	if err != nil {
//		klog.CtxErrorf(ctx, "[repo/rank_list] create rank list failed: err=%s", err)
//		return biz_error.CreateRankListErr
//	}
//	return nil
//}
//
//func (r *RankList) DeleteRankList(ctx context.Context, key, name string, businessId int64) *business_error.BusinessError {
//	err := redis_repo.RankListDao.Del(ctx, key, name, businessId)
//	if err != nil {
//		klog.CtxErrorf(ctx, "[repo/rank_list] delete rank list failed: err=%s", err)
//		return biz_error.DeleteRankListErr
//	}
//	return nil
//}
//
//func (r *RankList) UpdateRankList(ctx context.Context, key, name string, businessId int64, member string, delta float64) *business_error.BusinessError {
//	err := redis_repo.RankListDao.Update(ctx, key, name, businessId, member, delta)
//	if err != nil {
//		klog.CtxErrorf(ctx, "[repo/rank_list] update rank list failed: err=%s", err)
//		return biz_error.UpdateRankListErr
//	}
//	return nil
//}
//
//func (r *RankList) GetRankList(ctx context.Context, key, name string, businessId, start, length int64, isBigTop bool) (*entity.RankList, *business_error.BusinessError) {
//	zList, err := redis_repo.RankListDao.Get(ctx, key, name, businessId, start, length, isBigTop)
//	if err != nil {
//		klog.CtxErrorf(ctx, "[repo/rank_list] get rank list failed: err=%s", err)
//		return nil, biz_error.GetRankListErr
//	}
//	rankList := &entity.RankList{}
//	rankList.RankItems = ConvertToRankItems(zList)
//	return rankList, nil
//}
