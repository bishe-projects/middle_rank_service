package po

import "fmt"

type RankItem struct {
	Member string
	Score  float64
}

func (po *RankItem) String() string {
	return fmt.Sprintf("[member=%s, score=%f]", po.Member, po.Score)
}

type RankList struct {
	Key        string
	Name       string
	BusinessId int64
	RankItems  []*RankItem
}

func (po *RankList) String() string {
	return fmt.Sprintf("[key=%s, name=%s, businessId=%d, rankItems=%+v]", po.Key, po.Name, po.BusinessId, po.RankItems)
}
