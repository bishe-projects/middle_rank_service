package po

import (
	"fmt"
)

type RankConfig struct {
	Key        string `json:"key,omitempty"`
	BusinessId int64  `json:"business_id,omitempty"`
	Limit      int64  `json:"limit,omitempty"`
	IsBigTop   bool   `json:"is_big_top,omitempty"`
}

func (po *RankConfig) String() string {
	return fmt.Sprintf("[key=%s, businessId=%d, limit=%d, isBigTop=%t]", po.Key, po.BusinessId, po.Limit, po.IsBigTop)
}
