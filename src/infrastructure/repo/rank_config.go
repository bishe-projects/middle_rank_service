package repo

import (
	"context"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo/po"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo/redis_repo"
)

type RankConfigDao interface {
	Create(ctx context.Context, config *po.RankConfig) error
	Del(ctx context.Context, config *po.RankConfig) error
	Get(ctx context.Context, config *po.RankConfig) (*po.RankConfig, error)
	GetList(ctx context.Context, businessId int64) ([]string, error)
}

var RankConfigRepo = NewRankConfig(redis_repo.RankConfigDao)

type RankConfig struct {
	RankConfigDao RankConfigDao
}

func NewRankConfig(rankConfigDao RankConfigDao) *RankConfig {
	return &RankConfig{
		RankConfigDao: rankConfigDao,
	}
}

//var RankConfigRepo = new(RankConfig)
//
//type RankConfig struct{}
//
//func (r *RankConfig) Create(ctx context.Context, key string, businessId int64, config *entity.RankConfig) *business_error.BusinessError {
//	fields := r.ConvertToFields(config)
//	err := redis_repo.RankConfigDao.Create(ctx, key, businessId, fields)
//	if err != nil {
//		return biz_error.CreateRankConfigErr
//	}
//	return nil
//}
//
//func (r *RankConfig) ConvertToFields(config *entity.RankConfig) map[string]interface{} {
//	fields := make(map[string]interface{})
//	fields["key"] = config.Key
//	fields["limit"] = config.Limit
//	fields["isBigTop"] = config.IsBigTop
//	fields["sameScoreSortType"] = config.SameScoreSortType
//	return fields
//}
