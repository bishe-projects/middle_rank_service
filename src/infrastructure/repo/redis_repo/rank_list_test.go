package redis_repo

import (
	"context"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo/po"
	"reflect"
	"testing"
)

func TestRankList_Get(t *testing.T) {
	type args struct {
		ctx      context.Context
		rankList *po.RankList
		start    int64
		length   int64
	}
	tests := []struct {
		name    string
		args    args
		want    *po.RankList
		wantErr bool
	}{
		{
			name: "test",
			args: args{
				ctx: context.Background(),
				rankList: &po.RankList{
					Key:        "test_config",
					Name:       "test_rank",
					BusinessId: 1,
				},
				start:  0,
				length: 100,
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dao := &RankList{}
			got, err := dao.Get(tt.args.ctx, tt.args.rankList, tt.args.start, tt.args.length)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Get() got = %v, want %v", got, tt.want)
			}
		})
	}
}
