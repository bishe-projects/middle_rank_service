package redis_repo

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/cloudwego/kitex/pkg/klog"
	"github.com/go-redis/redis/v8"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo/po"
	"strconv"
)

var RankConfigDao = new(RankConfig)

const rankConfigListSuffix = "config_list"

type RankConfig struct{}

func (dao *RankConfig) Create(ctx context.Context, config *po.RankConfig) error {
	//scriptStr, err := readLua("lua/create_config.lua")
	//if err != nil {
	//	return err
	//}

	// use tostring(v) method to convert boolean to string
	scriptStr := `
		local configListKey = string.format("%s:config_list", ARGV[1])
		local count = redis.call("SAdd", configListKey, ARGV[2])
		if count == 0 then
			return -1
		end
		local configKey = string.format("%s:%s", ARGV[1], ARGV[2])
		local dict = cjson.decode(ARGV[3])
		local payload = {}
		for k, v in pairs(dict) do
			table.insert(payload, k)
			table.insert(payload, tostring(v))
		end
		local success = redis.call("HMSet", configKey, unpack(payload))
		if not success then
			return -2
		end
		return 0
	`
	script := redis.NewScript(scriptStr)
	fieldsJsonBytes, _ := json.Marshal(config)
	fieldsJsonStr := string(fieldsJsonBytes)
	res, err := script.Run(ctx, db, []string{}, fmt.Sprintf("%d", config.BusinessId), config.Key, fieldsJsonStr).Result()
	if err != nil {
		return err
	}
	klog.CtxInfof(ctx, "res=%+v", res)
	status, ok := res.(int64)
	if !ok {
		klog.CtxErrorf(ctx, "[redis_repo/rank_config] create config failed: err=%s, res=%+v", biz_error.LuaReturnConvertErr, res)
		return biz_error.LuaReturnConvertErr
	}
	if status == -1 {
		klog.CtxErrorf(ctx, "[redis_repo/rank_config] create config failed: err=%s", biz_error.ConfigExistsErr)
		return biz_error.ConfigExistsErr
	}
	if status == -2 {
		klog.CtxErrorf(ctx, "[redis_repo/rank_config] create config failed: err=%s", biz_error.ConfigMetaDataSetErr)
		return biz_error.ConfigMetaDataSetErr
	}
	return nil
}

func (dao *RankConfig) Del(ctx context.Context, config *po.RankConfig) error {
	//scriptStr, err := readLua("lua/del_config.lua")
	//if err != nil {
	//	return err
	//}
	scriptStr := `
		local listKey = string.format("%s:%s:rank_lists", ARGV[1], ARGV[2])
		local configKey = string.format("%s:%s", ARGV[1], ARGV[2])
		local configListKey = string.format("%s:config_list", ARGV[1])
		local keyList = redis.call("SMembers", listKey)
		for i=1, #keyList do
			local key = string.format("%s:%s:%s", ARGV[1], ARGV[2], keyList[i])
			redis.call("unlink", key)
		end
		redis.call("unlink", listKey)
		redis.call("unlink", configKey)
		redis.call("SRem", configListKey, ARGV[2])
		return 0
	`
	script := redis.NewScript(scriptStr)
	err := script.Run(ctx, db, []string{}, fmt.Sprintf("%d", config.BusinessId), config.Key).Err()
	if err != nil {
		return err
	}
	return nil
}

func (dao *RankConfig) Get(ctx context.Context, config *po.RankConfig) (*po.RankConfig, error) {
	result, err := db.HGetAll(ctx, dao.generateConfigKey(config.BusinessId, config.Key)).Result()
	if err != nil {
		return nil, err
	}
	config.Limit, _ = strconv.ParseInt(result["limit"], 10, 64)
	config.IsBigTop = result["is_big_top"] == "true"
	return config, nil
}

func (dao *RankConfig) GetList(ctx context.Context, businessId int64) ([]string, error) {
	return db.SMembers(ctx, dao.generateConfigListKey(businessId)).Result()
}

func (dao *RankConfig) generateConfigKey(businessId int64, key string) string {
	return fmt.Sprintf("%d:%s", businessId, key)
}

func (dao *RankConfig) generateConfigListKey(businessId int64) string {
	return fmt.Sprintf("%d:%s", businessId, rankConfigListSuffix)
}
