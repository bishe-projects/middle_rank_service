local listKey = string.format("%s:%s:rank_lists", ARGV[1], ARGV[2])
local configKey = string.format("%s:%s", ARGV[1], ARGV[2])
local configListKey = string.format("%s:config_list", ARGV[1])
local keyList = redis.call("SMembers", listKey)
for i=1, #keyList do
    local key = string.format("%s:%s:%s", ARGV[1], ARGV[2], keyList[i])
    redis.call("unlink", key)
end
redis.call("unlink", listKey)
redis.call("unlink", configKey)
redis.call("SRem", configListKey, ARGV[2])
return 0