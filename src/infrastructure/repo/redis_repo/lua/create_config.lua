local configListKey = string.format("%s:config_list", ARGV[1])
local count = redis.call("SAdd", configListKey, ARGV[2])
if count == 0 then
    return -1
end
local configKey = string.format("%s:%s", ARGV[1], ARGV[2])
local dict = cjson.decode(ARGV[3])
local payload = {}
for k, v in pairs(dict) do
    table.insert(payload, k)
    table.insert(payload, tostring(v))
end
local success = redis.call("HMSet", configKey, unpack(payload))
if not success then
    return -2
end
return 0