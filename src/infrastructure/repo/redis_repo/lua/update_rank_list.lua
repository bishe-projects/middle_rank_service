local rankListsKey = string.format("%s:%s:rank_lists", ARGV[1], ARGV[2])
local rankListKey = string.format("%s:%s:%s", ARGV[1], ARGV[2], ARGV[3])
local count = redis.call("SIsMember", rankListsKey, ARGV[3])
if count == 0 then
    return -1
end
local score = redis.call("ZScore", rankListKey, ARGV[4])
if score == false then
    redis.call("ZAdd", rankListKey, ARGV[5], ARGV[4])
else
    redis.call("ZAdd", rankListKey, ARGV[5] + score, ARGV[4])
end
return 0