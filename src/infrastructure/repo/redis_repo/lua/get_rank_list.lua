local rankListKey = string.format("%s:%s:%s", ARGV[1], ARGV[2], ARGV[3])
local configKey = string.format("%s:%s", ARGV[1], ARGV[2])
local limit = redis.call("HGet", configKey, "limit")
local isBigTop = redis.call("HGet", configKey, "is_big_top")
if limit == false or isBigTop == false then
    return -1
end
local start = tonumber(ARGV[4])
local len = tonumber(ARGV[5])
limit = tonumber(limit)
local stop = start + len
if stop >= limit then
    stop = limit - 1
end
if isBigTop == "true" then
    return redis.call("ZRevRange", rankListKey, start, stop, "WITHSCORES")
else
    return redis.call("ZRange", rankListKey, start, stop, "WITHSCORES")
end