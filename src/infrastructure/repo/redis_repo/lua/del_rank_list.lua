local rankListsKey = string.format("%s:%s:rank_lists", ARGV[1], ARGV[2])
local rankListKey = string.format("%s:%s:%s", ARGV[1], ARGV[2], ARGV[3])
local count = redis.call("SRem", rankListsKey, ARGV[3])
if count == 0 then
    return -1
end
redis.call("unlink", rankListKey)
return 0