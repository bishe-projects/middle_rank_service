local configKey = string.format("%s:%s", ARGV[1], ARGV[2])
local rankListsKey = string.format("%s:%s:rank_lists", ARGV[1], ARGV[2])
local exists = redis.call("Exists", configKey)
if exists == 0 then
    return -1
end
local count = redis.call("SAdd", rankListsKey, ARGV[3])
if count == 0 then
    return -2
end
return 0