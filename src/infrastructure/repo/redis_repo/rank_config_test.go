package redis_repo

import (
	"context"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo/po"
	"reflect"
	"testing"
)

func TestRankConfig_Get(t *testing.T) {
	type args struct {
		ctx    context.Context
		config *po.RankConfig
	}
	tests := []struct {
		name    string
		args    args
		want    map[string]string
		wantErr bool
	}{
		{
			name: "test",
			args: args{
				ctx: context.Background(),
				config: &po.RankConfig{
					Key:        "test_config",
					BusinessId: 4,
				},
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dao := &RankConfig{}
			got, err := dao.Get(tt.args.ctx, tt.args.config)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Get() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRankConfig_Create(t *testing.T) {
	type args struct {
		ctx    context.Context
		config *po.RankConfig
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "test",
			args: args{
				ctx: context.Background(),
				config: &po.RankConfig{
					Key:        "test_config",
					BusinessId: 4,
					Limit:      100,
					IsBigTop:   true,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dao := &RankConfig{}
			if err := dao.Create(tt.args.ctx, tt.args.config); (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
