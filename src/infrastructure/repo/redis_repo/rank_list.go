package redis_repo

import (
	"context"
	"fmt"
	"github.com/cloudwego/kitex/pkg/klog"
	"github.com/go-redis/redis/v8"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo/po"
	"strconv"
)

var RankListDao = new(RankList)

const rankListsSuffix = "rank_lists"

type RankList struct{}

func (dao *RankList) Create(ctx context.Context, rankList *po.RankList) error {
	//scriptStr, err := readLua("lua/create_rank_list.lua")
	//if err != nil {
	//	return err
	//}
	scriptStr := `
		local configKey = string.format("%s:%s", ARGV[1], ARGV[2])
		local rankListsKey = string.format("%s:%s:rank_lists", ARGV[1], ARGV[2])
		local exists = redis.call("Exists", configKey)
		if exists == 0 then
			return -1
		end
		local count = redis.call("SAdd", rankListsKey, ARGV[3])
		if count == 0 then
			return -2
		end
		return 0
	`
	script := redis.NewScript(scriptStr)
	res, err := script.Run(ctx, db, []string{}, fmt.Sprintf("%d", rankList.BusinessId), rankList.Key, rankList.Name).Result()
	if err != nil {
		return err
	}
	status, ok := res.(int64)
	if !ok {
		klog.CtxErrorf(ctx, "[redis_repo/rank_list] create rank list failed: err=%s, res=%+v", biz_error.LuaReturnConvertErr, res)
		return biz_error.LuaReturnConvertErr
	}
	if status == -1 {
		klog.CtxErrorf(ctx, "[redis_repo/rank_list] create rank list failed: err=%s", biz_error.ConfigNotExistsErr)
		return biz_error.ConfigNotExistsErr
	}
	if status == -2 {
		klog.CtxErrorf(ctx, "[redis_repo/rank_list] create rank list failed: err=%s", biz_error.RankListExistsErr)
		return biz_error.RankListExistsErr
	}
	return nil
}

func (dao *RankList) Del(ctx context.Context, rankList *po.RankList) error {
	//scriptStr, err := readLua("lua/del_rank_list.lua")
	//if err != nil {
	//	return err
	//}
	scriptStr := `
		local rankListsKey = string.format("%s:%s:rank_lists", ARGV[1], ARGV[2])
		local rankListKey = string.format("%s:%s:%s", ARGV[1], ARGV[2], ARGV[3])
		local count = redis.call("SRem", rankListsKey, ARGV[3])
		if count == 0 then
			return -1
		end
		redis.call("unlink", rankListKey)
		return 0
	`
	script := redis.NewScript(scriptStr)
	res, err := script.Run(ctx, db, []string{}, fmt.Sprintf("%d", rankList.BusinessId), rankList.Key, rankList.Name).Result()
	if err != nil {
		return err
	}
	status, ok := res.(int64)
	if !ok {
		klog.CtxErrorf(ctx, "[redis_repo/rank_list] del rank list failed: err=%s, res=%+v", biz_error.LuaReturnConvertErr, res)
		return biz_error.LuaReturnConvertErr
	}
	if status == -1 {
		klog.CtxErrorf(ctx, "[redis_repo/rank_list] del rank list failed: err=%s", biz_error.RankListNotExistsErr)
		return biz_error.RankListNotExistsErr
	}
	return nil
}

func (dao *RankList) Update(ctx context.Context, rankList *po.RankList, member string, delta float64) error {
	//scriptStr, err := readLua("lua/update_rank_list.lua")
	//if err != nil {
	//	return err
	//}
	scriptStr := `
		local rankListsKey = string.format("%s:%s:rank_lists", ARGV[1], ARGV[2])
		local rankListKey = string.format("%s:%s:%s", ARGV[1], ARGV[2], ARGV[3])
		local count = redis.call("SIsMember", rankListsKey, ARGV[3])
		if count == 0 then
			return -1
		end
		local score = redis.call("ZScore", rankListKey, ARGV[4])
		if score == false then
			redis.call("ZAdd", rankListKey, ARGV[5], ARGV[4])
		else
			redis.call("ZAdd", rankListKey, ARGV[5] + score, ARGV[4])
		end
		return 0
	`
	script := redis.NewScript(scriptStr)
	res, err := script.Run(ctx, db, []string{}, fmt.Sprintf("%d", rankList.BusinessId), rankList.Key, rankList.Name, member, delta).Result()
	klog.CtxInfof(ctx, "res=%v", res)
	if err != nil {
		return err
	}
	status, ok := res.(int64)
	if !ok {
		klog.CtxErrorf(ctx, "[redis_repo/rank_list] update rank list failed: err=%s, res=%+v", biz_error.LuaReturnConvertErr, res)
		return biz_error.LuaReturnConvertErr
	}
	if status == -1 {
		klog.CtxErrorf(ctx, "[redis_repo/rank_list] update rank list failed: err=%s", biz_error.RankListNotExistsErr)
		return biz_error.RankListNotExistsErr
	}
	return nil
}

func (dao *RankList) Get(ctx context.Context, rankList *po.RankList, start, length int64) (*po.RankList, error) {
	//scriptStr, err := readLua("lua/get_rank_list.lua")
	//if err != nil {
	//	return err
	//}
	scriptStr := `
		local rankListKey = string.format("%s:%s:%s", ARGV[1], ARGV[2], ARGV[3])
		local configKey = string.format("%s:%s", ARGV[1], ARGV[2])
		local limit = redis.call("HGet", configKey, "limit")
		local isBigTop = redis.call("HGet", configKey, "is_big_top")
		if limit == false or isBigTop == false then
			return -1
		end
		local start = tonumber(ARGV[4])
		local len = tonumber(ARGV[5])
		limit = tonumber(limit)
		local stop = start + len
		if stop >= limit then
			stop = limit - 1
		end
		if isBigTop == "true" then
			return redis.call("ZRevRange", rankListKey, start, stop, "WITHSCORES")
		else
			return redis.call("ZRange", rankListKey, start, stop, "WITHSCORES")
		end
	`
	script := redis.NewScript(scriptStr)
	res, err := script.Run(ctx, db, []string{}, fmt.Sprintf("%d", rankList.BusinessId), rankList.Key, rankList.Name, start, length).Result()
	if err != nil {
		return nil, err
	}
	if status, ok := res.(int64); ok {
		if status == -1 {
			klog.CtxErrorf(ctx, "[redis_repo/rank_list] get rank list failed: err=%s, res=%+v", biz_error.ConfigFieldNotFoundErr, res)
			return nil, biz_error.ConfigFieldNotFoundErr
		}
	}
	resList, ok := res.([]interface{})
	if !ok {
		klog.CtxErrorf(ctx, "[redis_repo/rank_list] get rank list failed: err=%s, res=%+v", biz_error.LuaReturnConvertErr, res)
		return nil, biz_error.LuaReturnConvertErr
	}
	rankItems := make([]*po.RankItem, 0, len(resList)/2)
	for i := 0; i < len(resList); i += 2 {
		member := resList[i].(string)
		scoreStr := resList[i+1].(string)
		score, err := strconv.ParseFloat(scoreStr, 10)
		if err != nil {
			klog.CtxErrorf(ctx, "[redis_repo/rank_list] get rank list failed: err=%s, score=%+v", err, scoreStr)
			return nil, err
		}
		rankItems = append(rankItems, &po.RankItem{Member: member, Score: score})
	}
	rankList.RankItems = rankItems
	return rankList, nil
}

func (dao *RankList) GetList(ctx context.Context, key string, businessId int64) ([]string, error) {
	return db.SMembers(ctx, dao.GenerateRankListsKey(businessId, key)).Result()
}

func (dao *RankList) RemoveMember(ctx context.Context, rankList *po.RankList, member string) error {
	return db.ZRem(ctx, dao.GenerateRankListKey(rankList.BusinessId, rankList.Key, rankList.Name), member).Err()
}

func (dao *RankList) GenerateRankListsKey(businessId int64, key string) string {
	return fmt.Sprintf("%d:%s:%s", businessId, key, rankListsSuffix)
}

func (dao *RankList) GenerateRankListKey(businessId int64, key, name string) string {
	return fmt.Sprintf("%d:%s:%s", businessId, key, name)
}
