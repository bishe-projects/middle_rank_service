package redis_repo

import (
	"github.com/go-redis/redis/v8"
	"io/ioutil"
)

var (
	db *redis.Client
)

func init() {
	db = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
}

func readLua(path string) (string, error) {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(f), nil
}
