package biz_error

import (
	"errors"
	"gitlab.com/bishe-projects/common_utils/business_error"
)

var (
	LuaReturnConvertErr    = errors.New("lua script return convert failed")
	ConfigExistsErr        = errors.New("this config already exists")
	ConfigMetaDataSetErr   = errors.New("config meta data set failed")
	ConfigNotExistsErr     = errors.New("config not exists")
	ConfigFieldNotFoundErr = errors.New("config field not found")
	RankListExistsErr      = errors.New("this rank list already exists")
	RankListNotExistsErr   = errors.New("rank list not exists")
)

var (
	CreateRankListErr    = business_error.NewBusinessError("create rank list failed", -10001)
	DeleteRankListErr    = business_error.NewBusinessError("delete rank list failed", -10002)
	UpdateRankListErr    = business_error.NewBusinessError("update rank list failed", -10003)
	GetRankListErr       = business_error.NewBusinessError("get rank list failed", -10004)
	GetRankListsErr      = business_error.NewBusinessError("get rank lists failed", -10005)
	CreateRankConfigErr  = business_error.NewBusinessError("create rank config failed", -10006)
	DeleteRankConfigErr  = business_error.NewBusinessError("delete rank config failed", -10007)
	GetRankConfigErr     = business_error.NewBusinessError("get rank config failed", -10008)
	GetRankConfigsErr    = business_error.NewBusinessError("get rank configs failed", -10009)
	RankListExistsBizErr = business_error.NewBusinessError("this rank list already exists", -10010)
	ConfigExistsBizErr   = business_error.NewBusinessError("this config already exists", -10011)
	RemoveMemberErr      = business_error.NewBusinessError("remove member from rank list failed", -10012)
)
