package service

import (
	"context"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/domain/rank_list/entity"
)

var RankListDomain = new(RankList)

type RankList struct{}

func (d *RankList) Create(ctx context.Context, rankList *entity.RankList) *business_error.BusinessError {
	return rankList.Create(ctx)
}

func (d *RankList) Del(ctx context.Context, rankList *entity.RankList) *business_error.BusinessError {
	return rankList.Del(ctx)
}

func (d *RankList) Update(ctx context.Context, rankList *entity.RankList, member string, delta float64) *business_error.BusinessError {
	return rankList.Update(ctx, member, delta)
}

func (d *RankList) Get(ctx context.Context, rankList *entity.RankList, start, length int64) (*entity.RankList, *business_error.BusinessError) {
	err := rankList.Get(ctx, start, length)
	return rankList, err
}

func (d *RankList) GetList(ctx context.Context, key string, businessId int64) ([]string, *business_error.BusinessError) {
	rankLists := new(entity.RankLists)
	return rankLists.Get(ctx, key, businessId)
}

func (d *RankList) RemoveMember(ctx context.Context, rankList *entity.RankList, member string) *business_error.BusinessError {
	return rankList.RemoveMember(ctx, member)
}
