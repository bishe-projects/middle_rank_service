package entity

import (
	"context"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"reflect"
	"testing"
)

func TestRankList_GetRankList(t *testing.T) {
	type fields struct {
		RankItems []*RankItem
	}
	type args struct {
		ctx        context.Context
		key        string
		name       string
		businessId int64
		start      int64
		length     int64
		isBigTop   bool
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *business_error.BusinessError
	}{
		{
			name:   "test",
			fields: fields{},
			args: args{
				ctx:        context.Background(),
				key:        "test_config",
				name:       "test_rank",
				businessId: 1,
				start:      0,
				length:     100,
				isBigTop:   true,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &RankList{
				RankItems: tt.fields.RankItems,
			}
			if got := r.GetRankList(tt.args.ctx, tt.args.key, tt.args.name, tt.args.businessId, tt.args.start, tt.args.length, tt.args.isBigTop); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetRankList() = %v, want %v", got, tt.want)
			}
		})
	}
}
