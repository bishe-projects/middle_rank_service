package entity

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo/po"
)

type RankItem struct {
	Member string
	Score  float64
}

type RankList struct {
	Key        string
	Name       string
	BusinessId int64
	RankItems  []*RankItem
}

func (r *RankList) Create(ctx context.Context) *business_error.BusinessError {
	err := repo.RankListRepo.RankListDao.Create(ctx, r.ConvertToPO())
	if err == biz_error.RankListExistsErr {
		return biz_error.RankListExistsBizErr
	}
	if err != nil {
		klog.CtxErrorf(ctx, "[entity/rank_list] create rank list failed: err=%s", err)
		return biz_error.CreateRankListErr
	}
	return nil
}

func (r *RankList) Del(ctx context.Context) *business_error.BusinessError {
	err := repo.RankListRepo.RankListDao.Del(ctx, r.ConvertToPO())
	if err != nil {
		klog.CtxErrorf(ctx, "[entity/rank_list] delete rank list failed: err=%s", err)
		return biz_error.DeleteRankListErr
	}
	return nil
}

func (r *RankList) Update(ctx context.Context, member string, delta float64) *business_error.BusinessError {
	err := repo.RankListRepo.RankListDao.Update(ctx, r.ConvertToPO(), member, delta)
	if err != nil {
		klog.CtxErrorf(ctx, "[entity/rank_list] update rank list failed: err=%s", err)
		return biz_error.UpdateRankListErr
	}
	return nil
}

func (r *RankList) Get(ctx context.Context, start, length int64) *business_error.BusinessError {
	rankListPO, err := repo.RankListRepo.RankListDao.Get(ctx, r.ConvertToPO(), start, length)
	if err != nil {
		klog.CtxErrorf(ctx, "[entity/rank_list] get rank list failed: err=%s", err)
		return biz_error.GetRankListErr
	}
	r.fillFromPO(rankListPO)
	return nil
}

func (r *RankList) RemoveMember(ctx context.Context, member string) *business_error.BusinessError {
	err := repo.RankListRepo.RankListDao.RemoveMember(ctx, r.ConvertToPO(), member)
	if err != nil {
		klog.CtxErrorf(ctx, "[entity/rank_list] remove member from rank list failed: err=%s", err)
		return biz_error.RemoveMemberErr
	}
	return nil
}

type RankLists []*RankList

func (r *RankLists) Get(ctx context.Context, key string, businessId int64) ([]string, *business_error.BusinessError) {
	nameList, err := repo.RankListRepo.RankListDao.GetList(ctx, key, businessId)
	if err != nil {
		klog.CtxErrorf(ctx, "[entity/rank_list] get rank lists failed: err=%s", err)
		return nil, biz_error.GetRankListsErr
	}
	return nameList, nil
}

// converter

func (r *RankItem) fillFromPO(po *po.RankItem) {
	r.Member = po.Member
	r.Score = po.Score
}

func (r *RankList) ConvertToPO() *po.RankList {
	return &po.RankList{
		Key:        r.Key,
		Name:       r.Name,
		BusinessId: r.BusinessId,
	}
}

func (r *RankList) fillFromPO(po *po.RankList) {
	r.RankItems = make([]*RankItem, 0, len(po.RankItems))
	for _, rankItemPO := range po.RankItems {
		rankItem := &RankItem{}
		rankItem.fillFromPO(rankItemPO)
		r.RankItems = append(r.RankItems, rankItem)
	}
}
