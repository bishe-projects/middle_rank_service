package service

import (
	"context"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/domain/rank_config/entity"
)

var RankConfigDomain = new(RankConfig)

type RankConfig struct{}

func (d *RankConfig) Create(ctx context.Context, config *entity.RankConfig) *business_error.BusinessError {
	return config.Create(ctx)
}

func (d *RankConfig) Del(ctx context.Context, config *entity.RankConfig) *business_error.BusinessError {
	return config.Del(ctx)
}

func (d *RankConfig) Get(ctx context.Context, config *entity.RankConfig) (*entity.RankConfig, *business_error.BusinessError) {
	err := config.Get(ctx)
	return config, err
}

func (d *RankConfig) GetList(ctx context.Context, businessId int64) ([]string, *business_error.BusinessError) {
	rankConfigs := new(entity.RankConfigs)
	return rankConfigs.Get(ctx, businessId)
}
