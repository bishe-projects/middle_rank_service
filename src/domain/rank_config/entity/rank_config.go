package entity

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/middle_rank_service/src/infrastructure/repo/po"
)

type RankConfig struct {
	Key        string
	BusinessId int64
	Limit      int64
	IsBigTop   bool
}

func (r *RankConfig) Create(ctx context.Context) *business_error.BusinessError {
	err := repo.RankConfigRepo.RankConfigDao.Create(ctx, r.ConvertToPO())
	if err == biz_error.ConfigExistsErr {
		return biz_error.ConfigExistsBizErr
	}
	if err != nil {
		klog.CtxErrorf(ctx, "[entity/rank_config] create rank config failed: err=%s", err)
		return biz_error.CreateRankConfigErr
	}
	return nil
}

func (r *RankConfig) Del(ctx context.Context) *business_error.BusinessError {
	err := repo.RankConfigRepo.RankConfigDao.Del(ctx, r.ConvertToPO())
	if err != nil {
		klog.CtxErrorf(ctx, "[entity/rank_config] delete rank config failed: err=%s", err)
		return biz_error.DeleteRankConfigErr
	}
	return nil
}

func (r *RankConfig) Get(ctx context.Context) *business_error.BusinessError {
	configPO, err := repo.RankConfigRepo.RankConfigDao.Get(ctx, r.ConvertToPO())
	if err != nil {
		klog.CtxErrorf(ctx, "[entity/rank_config] get rank config failed: err=%s", err)
		return biz_error.GetRankConfigErr
	}
	r.fillFromPO(configPO)
	return nil
}

type RankConfigs []*RankConfig

func (r *RankConfigs) Get(ctx context.Context, businessId int64) ([]string, *business_error.BusinessError) {
	nameList, err := repo.RankConfigRepo.RankConfigDao.GetList(ctx, businessId)
	if err != nil {
		klog.CtxErrorf(ctx, "[entity/rank_config] get rank config failed: err=%s", err)
		return nil, biz_error.GetRankConfigsErr
	}
	return nameList, nil
}

// converter

func (r *RankConfig) ConvertToPO() *po.RankConfig {
	return &po.RankConfig{
		Key:        r.Key,
		BusinessId: r.BusinessId,
		Limit:      r.Limit,
		IsBigTop:   r.IsBigTop,
	}
}

func (r *RankConfig) fillFromPO(po *po.RankConfig) {
	r.Limit = po.Limit
	r.IsBigTop = po.IsBigTop
}
