package main

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/middle_rank_service/src/interface/facade"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"
)

// RankServiceImpl implements the last service interface defined in the IDL.
type RankServiceImpl struct{}

// GetRankConfigs implements the RankServiceImpl interface.
func (s *RankServiceImpl) GetRankConfigs(ctx context.Context, req *rank.GetRankConfigsReq) (resp *rank.GetRankConfigsResp, err error) {
	klog.CtxInfof(ctx, "GetRankConfigs req=%+v", req)
	resp = facade.RankConfigFacade.GetList(ctx, req)
	klog.CtxInfof(ctx, "GetRankConfigs resp=%+v", resp)
	return
}

// CreateRankConfig implements the RankServiceImpl interface.
func (s *RankServiceImpl) CreateRankConfig(ctx context.Context, req *rank.CreateRankConfigReq) (resp *rank.CreateRankConfigResp, err error) {
	klog.CtxInfof(ctx, "CreateRankConfig req=%+v", req)
	resp = facade.RankConfigFacade.Create(ctx, req)
	klog.CtxInfof(ctx, "CreateRankConfig resp=%+v", resp)
	return
}

// DeleteRankConfig implements the RankServiceImpl interface.
func (s *RankServiceImpl) DeleteRankConfig(ctx context.Context, req *rank.DeleteRankConfigReq) (resp *rank.DeleteRankConfigResp, err error) {
	klog.CtxInfof(ctx, "DeleteRankConfig req=%+v", req)
	resp = facade.RankConfigFacade.Del(ctx, req)
	klog.CtxInfof(ctx, "DeleteRankConfig resp=%+v", resp)
	return
}

// GetRankConfig implements the RankServiceImpl interface.
func (s *RankServiceImpl) GetRankConfig(ctx context.Context, req *rank.GetRankConfigReq) (resp *rank.GetRankConfigResp, err error) {
	klog.CtxInfof(ctx, "GetRankConfig req=%+v", req)
	resp = facade.RankConfigFacade.Get(ctx, req)
	klog.CtxInfof(ctx, "GetRankConfig resp=%+v", resp)
	return
}

// GetRankLists implements the RankServiceImpl interface.
func (s *RankServiceImpl) GetRankLists(ctx context.Context, req *rank.GetRankListsReq) (resp *rank.GetRankListsResp, err error) {
	klog.CtxInfof(ctx, "GetRankLists req=%+v", req)
	resp = facade.RankListFacade.GetList(ctx, req)
	klog.CtxInfof(ctx, "GetRankLists resp=%+v", resp)
	return
}

// CreateRankList implements the RankServiceImpl interface.
func (s *RankServiceImpl) CreateRankList(ctx context.Context, req *rank.CreateRankListReq) (resp *rank.CreateRankListResp, err error) {
	klog.CtxInfof(ctx, "CreateRankList req=%+v", req)
	resp = facade.RankListFacade.Create(ctx, req)
	klog.CtxInfof(ctx, "CreateRankList resp=%+v", resp)
	return
}

// UpdateRankList implements the RankServiceImpl interface.
func (s *RankServiceImpl) UpdateRankList(ctx context.Context, req *rank.UpdateRankListReq) (resp *rank.UpdateRankListResp, err error) {
	klog.CtxInfof(ctx, "UpdateRankList req=%+v", req)
	resp = facade.RankListFacade.Update(ctx, req)
	klog.CtxInfof(ctx, "UpdateRankList resp=%+v", resp)
	return
}

// DeleteRankList implements the RankServiceImpl interface.
func (s *RankServiceImpl) DeleteRankList(ctx context.Context, req *rank.DeleteRankListReq) (resp *rank.DeleteRankListResp, err error) {
	klog.CtxInfof(ctx, "DeleteRankList req=%+v", req)
	resp = facade.RankListFacade.Del(ctx, req)
	klog.CtxInfof(ctx, "DeleteRankList resp=%+v", resp)
	return
}

// GetRankList implements the RankServiceImpl interface.
func (s *RankServiceImpl) GetRankList(ctx context.Context, req *rank.GetRankListReq) (resp *rank.GetRankListResp, err error) {
	klog.CtxInfof(ctx, "GetRankList req=%+v", req)
	resp = facade.RankListFacade.Get(ctx, req)
	klog.CtxInfof(ctx, "GetRankList resp=%+v", resp)
	return
}

// RemoveRankListMember implements the RankServiceImpl interface.
func (s *RankServiceImpl) RemoveRankListMember(ctx context.Context, req *rank.RemoveRankListMemberReq) (resp *rank.RemoveRankListMemberResp, err error) {
	klog.CtxInfof(ctx, "RemoveRankListMember req=%+v", req)
	resp = facade.RankListFacade.RemoveMember(ctx, req)
	klog.CtxInfof(ctx, "RemoveRankListMember resp=%+v", resp)
	return
}
