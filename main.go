package main

import (
	"github.com/cloudwego/kitex/server"
	rank "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank/rankservice"
	"log"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:8886")
	svr := rank.NewServer(new(RankServiceImpl), server.WithServiceAddr(addr))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
