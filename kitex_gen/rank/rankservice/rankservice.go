// Code generated by Kitex v0.3.1. DO NOT EDIT.

package rankservice

import (
	"context"
	"github.com/cloudwego/kitex/client"
	kitex "github.com/cloudwego/kitex/pkg/serviceinfo"
	"gitlab.com/bishe-projects/middle_rank_service/kitex_gen/rank"
)

func serviceInfo() *kitex.ServiceInfo {
	return rankServiceServiceInfo
}

var rankServiceServiceInfo = NewServiceInfo()

func NewServiceInfo() *kitex.ServiceInfo {
	serviceName := "RankService"
	handlerType := (*rank.RankService)(nil)
	methods := map[string]kitex.MethodInfo{
		"getRankConfigs":       kitex.NewMethodInfo(getRankConfigsHandler, newRankServiceGetRankConfigsArgs, newRankServiceGetRankConfigsResult, false),
		"createRankConfig":     kitex.NewMethodInfo(createRankConfigHandler, newRankServiceCreateRankConfigArgs, newRankServiceCreateRankConfigResult, false),
		"deleteRankConfig":     kitex.NewMethodInfo(deleteRankConfigHandler, newRankServiceDeleteRankConfigArgs, newRankServiceDeleteRankConfigResult, false),
		"getRankConfig":        kitex.NewMethodInfo(getRankConfigHandler, newRankServiceGetRankConfigArgs, newRankServiceGetRankConfigResult, false),
		"getRankLists":         kitex.NewMethodInfo(getRankListsHandler, newRankServiceGetRankListsArgs, newRankServiceGetRankListsResult, false),
		"createRankList":       kitex.NewMethodInfo(createRankListHandler, newRankServiceCreateRankListArgs, newRankServiceCreateRankListResult, false),
		"updateRankList":       kitex.NewMethodInfo(updateRankListHandler, newRankServiceUpdateRankListArgs, newRankServiceUpdateRankListResult, false),
		"deleteRankList":       kitex.NewMethodInfo(deleteRankListHandler, newRankServiceDeleteRankListArgs, newRankServiceDeleteRankListResult, false),
		"getRankList":          kitex.NewMethodInfo(getRankListHandler, newRankServiceGetRankListArgs, newRankServiceGetRankListResult, false),
		"removeRankListMember": kitex.NewMethodInfo(removeRankListMemberHandler, newRankServiceRemoveRankListMemberArgs, newRankServiceRemoveRankListMemberResult, false),
	}
	extra := map[string]interface{}{
		"PackageName": "rank",
	}
	svcInfo := &kitex.ServiceInfo{
		ServiceName:     serviceName,
		HandlerType:     handlerType,
		Methods:         methods,
		PayloadCodec:    kitex.Thrift,
		KiteXGenVersion: "v0.3.1",
		Extra:           extra,
	}
	return svcInfo
}

func getRankConfigsHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*rank.RankServiceGetRankConfigsArgs)
	realResult := result.(*rank.RankServiceGetRankConfigsResult)
	success, err := handler.(rank.RankService).GetRankConfigs(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newRankServiceGetRankConfigsArgs() interface{} {
	return rank.NewRankServiceGetRankConfigsArgs()
}

func newRankServiceGetRankConfigsResult() interface{} {
	return rank.NewRankServiceGetRankConfigsResult()
}

func createRankConfigHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*rank.RankServiceCreateRankConfigArgs)
	realResult := result.(*rank.RankServiceCreateRankConfigResult)
	success, err := handler.(rank.RankService).CreateRankConfig(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newRankServiceCreateRankConfigArgs() interface{} {
	return rank.NewRankServiceCreateRankConfigArgs()
}

func newRankServiceCreateRankConfigResult() interface{} {
	return rank.NewRankServiceCreateRankConfigResult()
}

func deleteRankConfigHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*rank.RankServiceDeleteRankConfigArgs)
	realResult := result.(*rank.RankServiceDeleteRankConfigResult)
	success, err := handler.(rank.RankService).DeleteRankConfig(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newRankServiceDeleteRankConfigArgs() interface{} {
	return rank.NewRankServiceDeleteRankConfigArgs()
}

func newRankServiceDeleteRankConfigResult() interface{} {
	return rank.NewRankServiceDeleteRankConfigResult()
}

func getRankConfigHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*rank.RankServiceGetRankConfigArgs)
	realResult := result.(*rank.RankServiceGetRankConfigResult)
	success, err := handler.(rank.RankService).GetRankConfig(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newRankServiceGetRankConfigArgs() interface{} {
	return rank.NewRankServiceGetRankConfigArgs()
}

func newRankServiceGetRankConfigResult() interface{} {
	return rank.NewRankServiceGetRankConfigResult()
}

func getRankListsHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*rank.RankServiceGetRankListsArgs)
	realResult := result.(*rank.RankServiceGetRankListsResult)
	success, err := handler.(rank.RankService).GetRankLists(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newRankServiceGetRankListsArgs() interface{} {
	return rank.NewRankServiceGetRankListsArgs()
}

func newRankServiceGetRankListsResult() interface{} {
	return rank.NewRankServiceGetRankListsResult()
}

func createRankListHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*rank.RankServiceCreateRankListArgs)
	realResult := result.(*rank.RankServiceCreateRankListResult)
	success, err := handler.(rank.RankService).CreateRankList(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newRankServiceCreateRankListArgs() interface{} {
	return rank.NewRankServiceCreateRankListArgs()
}

func newRankServiceCreateRankListResult() interface{} {
	return rank.NewRankServiceCreateRankListResult()
}

func updateRankListHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*rank.RankServiceUpdateRankListArgs)
	realResult := result.(*rank.RankServiceUpdateRankListResult)
	success, err := handler.(rank.RankService).UpdateRankList(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newRankServiceUpdateRankListArgs() interface{} {
	return rank.NewRankServiceUpdateRankListArgs()
}

func newRankServiceUpdateRankListResult() interface{} {
	return rank.NewRankServiceUpdateRankListResult()
}

func deleteRankListHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*rank.RankServiceDeleteRankListArgs)
	realResult := result.(*rank.RankServiceDeleteRankListResult)
	success, err := handler.(rank.RankService).DeleteRankList(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newRankServiceDeleteRankListArgs() interface{} {
	return rank.NewRankServiceDeleteRankListArgs()
}

func newRankServiceDeleteRankListResult() interface{} {
	return rank.NewRankServiceDeleteRankListResult()
}

func getRankListHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*rank.RankServiceGetRankListArgs)
	realResult := result.(*rank.RankServiceGetRankListResult)
	success, err := handler.(rank.RankService).GetRankList(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newRankServiceGetRankListArgs() interface{} {
	return rank.NewRankServiceGetRankListArgs()
}

func newRankServiceGetRankListResult() interface{} {
	return rank.NewRankServiceGetRankListResult()
}

func removeRankListMemberHandler(ctx context.Context, handler interface{}, arg, result interface{}) error {
	realArg := arg.(*rank.RankServiceRemoveRankListMemberArgs)
	realResult := result.(*rank.RankServiceRemoveRankListMemberResult)
	success, err := handler.(rank.RankService).RemoveRankListMember(ctx, realArg.Req)
	if err != nil {
		return err
	}
	realResult.Success = success
	return nil
}
func newRankServiceRemoveRankListMemberArgs() interface{} {
	return rank.NewRankServiceRemoveRankListMemberArgs()
}

func newRankServiceRemoveRankListMemberResult() interface{} {
	return rank.NewRankServiceRemoveRankListMemberResult()
}

type kClient struct {
	c client.Client
}

func newServiceClient(c client.Client) *kClient {
	return &kClient{
		c: c,
	}
}

func (p *kClient) GetRankConfigs(ctx context.Context, req *rank.GetRankConfigsReq) (r *rank.GetRankConfigsResp, err error) {
	var _args rank.RankServiceGetRankConfigsArgs
	_args.Req = req
	var _result rank.RankServiceGetRankConfigsResult
	if err = p.c.Call(ctx, "getRankConfigs", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) CreateRankConfig(ctx context.Context, req *rank.CreateRankConfigReq) (r *rank.CreateRankConfigResp, err error) {
	var _args rank.RankServiceCreateRankConfigArgs
	_args.Req = req
	var _result rank.RankServiceCreateRankConfigResult
	if err = p.c.Call(ctx, "createRankConfig", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) DeleteRankConfig(ctx context.Context, req *rank.DeleteRankConfigReq) (r *rank.DeleteRankConfigResp, err error) {
	var _args rank.RankServiceDeleteRankConfigArgs
	_args.Req = req
	var _result rank.RankServiceDeleteRankConfigResult
	if err = p.c.Call(ctx, "deleteRankConfig", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetRankConfig(ctx context.Context, req *rank.GetRankConfigReq) (r *rank.GetRankConfigResp, err error) {
	var _args rank.RankServiceGetRankConfigArgs
	_args.Req = req
	var _result rank.RankServiceGetRankConfigResult
	if err = p.c.Call(ctx, "getRankConfig", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetRankLists(ctx context.Context, req *rank.GetRankListsReq) (r *rank.GetRankListsResp, err error) {
	var _args rank.RankServiceGetRankListsArgs
	_args.Req = req
	var _result rank.RankServiceGetRankListsResult
	if err = p.c.Call(ctx, "getRankLists", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) CreateRankList(ctx context.Context, req *rank.CreateRankListReq) (r *rank.CreateRankListResp, err error) {
	var _args rank.RankServiceCreateRankListArgs
	_args.Req = req
	var _result rank.RankServiceCreateRankListResult
	if err = p.c.Call(ctx, "createRankList", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) UpdateRankList(ctx context.Context, req *rank.UpdateRankListReq) (r *rank.UpdateRankListResp, err error) {
	var _args rank.RankServiceUpdateRankListArgs
	_args.Req = req
	var _result rank.RankServiceUpdateRankListResult
	if err = p.c.Call(ctx, "updateRankList", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) DeleteRankList(ctx context.Context, req *rank.DeleteRankListReq) (r *rank.DeleteRankListResp, err error) {
	var _args rank.RankServiceDeleteRankListArgs
	_args.Req = req
	var _result rank.RankServiceDeleteRankListResult
	if err = p.c.Call(ctx, "deleteRankList", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) GetRankList(ctx context.Context, req *rank.GetRankListReq) (r *rank.GetRankListResp, err error) {
	var _args rank.RankServiceGetRankListArgs
	_args.Req = req
	var _result rank.RankServiceGetRankListResult
	if err = p.c.Call(ctx, "getRankList", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}

func (p *kClient) RemoveRankListMember(ctx context.Context, req *rank.RemoveRankListMemberReq) (r *rank.RemoveRankListMemberResp, err error) {
	var _args rank.RankServiceRemoveRankListMemberArgs
	_args.Req = req
	var _result rank.RankServiceRemoveRankListMemberResult
	if err = p.c.Call(ctx, "removeRankListMember", &_args, &_result); err != nil {
		return
	}
	return _result.GetSuccess(), nil
}
